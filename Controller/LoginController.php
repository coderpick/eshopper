<?php
$filepath = realpath(dirname(__FILE__));
include_once ($filepath.'/../database/DBController.php');
include_once ($filepath.'/../helper/Helper.php');

class LoginController
{
    private $db;
    private $helper;

    public function __construct()
    {
        $this->db = new DBController();
        $this->helper = new Helper();
    }

    public function adminLogin($email,$password)
    {
        $email      = $this->helper->validation($email);
        $password   = $this->helper->validation($password);

        $email      = mysqli_real_escape_string($this->db->conn, $email);
        $password   = mysqli_real_escape_string($this->db->conn, $password);

        if (empty($email) || empty($password)) {
            $loginmsg = 'Username or Password must not be empty !';
            return $loginmsg;
        } else {

            $query = "SELECT * FROM users WHERE email='$email' AND password = md5('$password')";

            $result = $this->db->select($query);

            if ($result != false) {
                $value = $result->fetch_assoc();

                Session::set('login', true);
                Session::set('userID', $value['id']);
                Session::set('userName', $value['name']);
                echo '<script>window.location="dashboard.php"</script>';
            } else {
                $loginmsg = '<strong>Error!</strong> Username or Password not match !';
                return $loginmsg;
            }
        }
    }

}