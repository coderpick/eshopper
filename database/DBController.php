<?php 

	$filepath = realpath(dirname(__FILE__));
	include ($filepath.'/../config/config.php');

	class DBController{

		private $host 		= DB_HOST;
		private $db_user 	= DB_USER;
		private $password 	= DB_PASSWORD;
		private $db_name 	= DB_NAME;

		public $conn;
		public $error;

		public function __construct()
		{
			$this->connection();
		}

		public function connection()
		{
			$this->conn = new mysqli($this->host,$this->db_user,$this->password,$this->db_name);

			if(!$this->conn)
			{
				$this->error = 'Database connection failed'.$this->conn->mysqli_error;
			}
			
		}

        public function select($query){
            $result = $this->conn->query($query) or die($this->conn->error.__LINE__);
            if($result->num_rows > 0){
                return $result;
            } else {
                return false;
            }
        }

	}
 ?>